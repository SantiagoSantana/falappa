# Explanations, belief revision and defeasible reasoning

## Revision de creencias

**Modelar creeencias y conocimiento de forma dinámica**

¿Cómo se debería incorporar nuevas creencias?
¿Cómo mantener la consistencia de la base de creencias?

### Frameworks de Revision de Creencias

#### AGM

Postulado **EXITO** uno de los mas controversiales, ya que se acepta toda informacion nueva

Estado epistémico
- Conjuntos de creencias
- Bases de creeencias

2 formas de representar operadores
- construccion explicitca (algoritmo)
- postulados racionales

Funciones de seleccion
- Partial Meet Contraction
- Conjunto de Kernels

#### FKS - 2002
Propuesto en este paper
Explicado en las siguientes diapositivas

## Operador de Revisión No Priorizado (FKS 2002)

**&omicron;** omicron

Operador intermedio entre **Semi-Revision** y **Merge**

La nueva informacion solo es aceptada cuando satisface una **explicación**

Por lo tanto algunas veces se **acepta** y otras veces se **rechaza**

Una explicacion consta de: **explanans** y **explanandum**

Las creencias a ser eliminadas se guardan en un conjunto de creencias a ser anuladas

### Explanans

Creencias que son evaluadas y se determinará en una conclusion

### Explanandum

Conclusion final obtenida al aplicar el operador de revision

## Ejemplo de Revision de Creencias

Michael tiene las siguientes creencias:

&alpha; = Todas las aves vuelan
&beta; = Tweety es un ave
&delta; = Tweey vuela

Pero Joahana dice:

¬&delta; = Tweety no vuela

Para que Michael incorpore la nueva creencia debe solicitar explicaciones que avalen ese cambio

Johana justifica ¬c con las siguientes creencias

&gamma; = Tweety es un pinguino
&epsilon; = Los pinguinos no vuelan

**El objetivo principal de una EXPLICACION es racionalizar hechos**

## Definicion 1

*El conjunto A es una **explicación** para la sentencia α si y sólo si se satisfacen las siguientes propiedades*

|Propiedad|Formula|Descripcion|
|-|-|-|
| Deducción | A &#8866; α | La explicacion implica explanandum|
| Consistencia | A no &#8866; ⊥ | Evita la posiblidad de la explicacion resulte en una inconsistencia|
| Minimalidad | si B ⊂ A entonces B no &#8866; α | La creencias irrelevantes no se tienen en cuenta en explanans |
| Contenido Informacional | Cn(A) no &sube; Cn(α) | El explanandum deberia poder implicar cualquier sentencia de explanans |

## Postulados

### Inclusión

**K◦A ⊆ K ∪ A**

Si un agente revisa su base de creencias K con respecto a un conjunto A, entonces el nuevo conjunto será contenido en la unión de K con A

### Vacuidad

** Si K ∪ A no &#8866; ⊥ then K◦A = K ∪ A **

Si la entrada del conjunto A es consistente con las creeencias originales de K, entonces la base de creencias revisadas es igual a la union de K con A.

### Consistencia Fuerte

** K◦A not &#8866; ⊥ **

Asegura la consistencia en la base de creencias revisada

### Retencion Nucleo

** Si α ∈ (K ∪ A) \ (K◦A) entonces existe un conjunto H tal que H ⊆ (K ∪ A) **

H es consistente pero H &cup; {&alpha;} es incosistente
Este postulado establece la intucion de que nada es removido de la union entre la base de creencias revisadas y las entradas, a menos que contribuya a dejar el nuevo conjunto de  creencias en estado consistente.

### Relevancia

** Si α ∈ (K ∪ A) \ (K◦A) entonces existe un conjunto H tal que K◦A ⊆ H ⊆ (K ∪ A)**

H es consistente pero H &cup; {&alpha;} es incosistente.
Este postulado es una version mas fuerte de Retencion Nucleo y es usada para caracterizar algunas clases de operadores de revision.

### Reversion

** Si K ∪ A y K ∪ B tienen algun subconjunto minimamente inconsistente entonces (K ∪ A) \ (K◦A) = (K ∪ B) \ (K◦B) **

Si K ∪ A y K ∪ B tienen algun subconjunto minimamente inconsistente entonces las sentencias eliminadas en las respectivas revisiones de A y B son las mismas.

### Justicia

Si la condicion que A not &#8866; ⊥, B not &#8866; ⊥ y para todos los H ⊆ K se mantiene que (H ∪ A) &#8866; ⊥ si y solo si (H ∪ B) &#8866; ⊥, then (K ∪ A) \ (K◦A) = (K ∪ B) \ (K◦B)

Si cualquier subconjunto H de K es incosistente con el conjunto consistente A si y sólo si es inconsistente con el conjunto consiste B, entonces las sentencias eliminadas en las respectivas revisiones con respecto a A y B son las mismas.
