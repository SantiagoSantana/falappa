# Explanations, belief revision and defeasible reasoning

## Revision de creencias

**Modelar creeencias y conocimiento de forma dinámica**

¿Cómo se debería incorporar nuevas creencias?
¿Cómo mantener la consistencia de la base de creencias?

### Frameworks de Revision de Creencias

#### AGM

Postulado **EXITO** uno de los mas controversiales, ya que se acepta toda informacion nueva

Estado epistémico
- Conjuntos de creencias
- Bases de creeencias

2 formas de representar operadores
- construccion explicitca (algoritmo)
- postulados racionales

Funciones de seleccion
- Partial Meet Contraction
- Conjunto de Kernels

#### FKS - 2002
Propuesto en este paper
Explicado en las siguientes diapositivas

## Operador de Revisión No Priorizado (FKS 2002)

**&omicron;** omicron

Operador intermedio entre **Semi-Revision** y **Merge**

La nueva informacion solo es aceptada cuando satisface una **explicación**

Por lo tanto algunas veces se **acepta** y otras veces se **rechaza**

Una explicacion consta de: **explanans** y **explanandum**

Las creencias a ser eliminadas se guardan en un conjunto de creencias a ser anuladas

### Explanans

Creencias que son evaluadas y se determinará en una conclusion

### Explanandum

Conclusion final obtenida al aplicar el operador de revision

## Ejemplo de Revision de Creencias

Michael tiene las siguientes creencias:

&alpha; = Todas las aves vuelan
&beta; = Tweety es un ave
&delta; = Tweey vuela

Pero Joahana dice:

¬&delta; = Tweety no vuela

Para que Michael incorpore la nueva creencia debe solicitar explicaciones que avalen ese cambio

Johana justifica ¬c con las siguientes creencias

&gamma; = Tweety es un pinguino
&epsilon; = Los pinguinos no vuelan

**El objetivo principal de una EXPLICACION es racionalizar hechos**

## Definicion 1

*El conjunto A es una **explicación** para la sentencia α si y sólo si se satisfacen las siguientes propiedades*

|Propiedad|Formula|Descripcion|
|-|-|-|
| Deducción | A &#8866; α | La explicacion implica explanandum|
| Consistencia | A no &#8866; ⊥ | Evita la posiblidad de la explicacion resulte en una inconsistencia|
| Minimalidad | si B ⊂ A entonces B no &#8866; α | La creencias irrelevantes no se tienen en cuenta en explanans |
| Contenido Informacional | Cn(A) no &sube; Cn(α) | El explanandum deberia poder implicar cualquier sentencia de explanans |

## Postulados para Explanans

### Inclusión

**K◦A ⊆ K ∪ A**

Si un agente revisa su base de creencias K con respecto a un conjunto A, entonces el nuevo conjunto será contenido en la unión de K con A

### Vacuidad

** Si K ∪ A no &#8866; ⊥ then K◦A = K ∪ A **

Si la entrada del conjunto A es consistente con las creeencias originales de K, entonces la base de creencias revisadas es igual a la union de K con A.

### Consistencia Fuerte

** K◦A not &#8866; ⊥ **

Asegura la consistencia en la base de creencias revisada

### Retencion Nucleo

** Si α ∈ (K ∪ A) \ (K◦A) entonces existe un conjunto H tal que H ⊆ (K ∪ A) **

H es consistente pero H &cup; {&alpha;} es incosistente
Este postulado establece la intucion de que nada es removido de la union entre la base de creencias revisadas y las entradas, a menos que contribuya a dejar el nuevo conjunto de  creencias en estado consistente.

### Relevancia

** Si α ∈ (K ∪ A) \ (K◦A) entonces existe un conjunto H tal que K◦A ⊆ H ⊆ (K ∪ A)**

H es consistente pero H &cup; {&alpha;} es incosistente.
Este postulado es una version mas fuerte de Retencion Nucleo y es usada para caracterizar algunas clases de operadores de revision.

### Reversion

** Si K ∪ A y K ∪ B tienen algun subconjunto minimamente inconsistente entonces (K ∪ A) \ (K◦A) = (K ∪ B) \ (K◦B) **

Si K ∪ A y K ∪ B tienen algun subconjunto minimamente inconsistente entonces las sentencias eliminadas en las respectivas revisiones de A y B son las mismas.

### Justicia

Si la condicion que A not &#8866; ⊥, B not &#8866; ⊥ y para todos los H ⊆ K se mantiene que (H ∪ A) &#8866; ⊥ si y solo si (H ∪ B) &#8866; ⊥, then (K ∪ A) \ (K◦A) = (K ∪ B) \ (K◦B)

Si cualquier subconjunto H de K es incosistente con el conjunto consistente A si y sólo si es inconsistente con el conjunto consiste B, entonces las sentencias eliminadas en las respectivas revisiones con respecto a A y B son las mismas.

## Kernel Contractions

### Definicion 3

*Sea K un conjunto de sentencias y α una sentencia. Entonces  K^⊥⊥α es el conjutno de todos los K' tal que K' ∈ K^⊥⊥α si y sólo si si K' ⊆ K, K' &#8866; α, y si K'' ⊂ K' entonces K'' not &#8866; α. El conjunto  K^⊥⊥α es llamado el conjunto de kernels, y sus elementos son llamados α-kernels de K.*

#### Ejemplo

Si K = {p,p → q, r, r → s, r ∧ s → q,t → u} entonces el conjunto de q-kernels es igual a {{p,p → q}, {r, r → s, r ∧ s → q}}

Si K = {p,p → q} entonces  K^⊥⊥(p → p) = {∅} porque p → p ∈ Cn(∅) y K^⊥⊥¬p = ∅ ya que K not &#8866; ¬p

### Definicion 4

*Sea K un conjunto de sentencias. Una funcion de incision externa para K es “σ”(σ : 2^2L ⇒ 2^L) tal que para cualquier conjunto A ⊆ L, se mantiene lo siguiente :*

(1) σ ((K ∪ A)^⊥⊥ ⊥) ⊆ ∪((K ∪ A)^⊥⊥ ⊥)
(2) Si X ∈ (K ∪ A)^⊥⊥ ⊥ y X not = ∅ entonces (X ∩ σ ((K ∪ A)⊥⊥⊥)) not = ∅.

#### Ejemplo

Tomando K = {t, u, r, r → s} y A = {¬t,p,p → ¬s} entonces K ∪ A = {t, u, r, r → s, ¬t,p,p → ¬s}, (K ∪A)^⊥⊥ ⊥ = {{r, r → s,p,p → ¬s}, {t, ¬t}}, y algunos resultado posibles de σ ((K ∪ A)^⊥⊥ ⊥) son {p,t}, {p, ¬t} y {p → ¬s,t}

### Definicion 5

*Sean K y A conjuntos de sentencias y “σ ” una funcion de incision externa para K. El operador de revision de creencias "&omicron;" para el conjunto de sentencias (&omicron; : 2^L × 2^L ⇒ 2^L) es definido como K &omicron; A = (K ∪ A) \ σ ((K ∪ A)^⊥⊥ ⊥).*

El mecanismo de este operador es añadir A a K Y eliminar del resultado todas las inconsistencias posibles por lo que la funcion de incision realiza un corte sobre cada subconjunto minimamente inconsistente de K ∪ A. Ya que este operador usa una  funcion de incision y el conjunto de ⊥-kernels, se lo denomina revision kernel para el conjunto de sentencias.

### Teorema 6

*Sea K base de creencias. El operador "o" es ua revision de kernes para el conjunto de sentencias si y sólo si satisface **inclusion**, **consistencia fuerte**, **retencion nucleo** y **reversion***

#### Inclusion

Se prueba por definicion

#### Consistencia fuerte

Ya que todos los conjuntos en (K ∪ A)^⊥⊥ ⊥ son minimamente inconsistentes, y los cortes σ en cada conjunto, entonces (K ∪ A) \ σ ((K ∪ A)^⊥⊥ ⊥) es consistente

#### Retencion nucleo

Suponer que α ∈ (K ∪ A) \ (K◦σA). Esto es, α ∈ K ∪ A y α / ∈ K◦σA. Entonces α ∈ σ ((K ∪ A)^⊥⊥ ⊥).  σ ((K ∪ A)⊥⊥⊥) ⊆ ((K ∪ A)⊥⊥⊥)
there is some X such that α ∈ X and X ∈ (K ∪ A)⊥⊥⊥. Let Y = X \ {α}. Then
there is some Y such that Y ⊆ (K ∪ A), Y  ⊥ but Y ∪ {α}  ⊥. Therefore, core
retainment is satisfied

#### Reversion

Suppose that K ∪A and K ∪B have the same minimally inconsistent subsets.
That means that (K ∪ A)⊥⊥⊥ = (K ∪ B)⊥⊥⊥. Since σ is a well defined function
then σ ((K ∪ A)⊥⊥⊥) = σ ((K ∪ B)⊥⊥⊥). We need to show that
(K ∪ A) \ (K◦σA) = (K ∪ B) \ (K◦σB).
(⊆) If α ∈ (K ∪ A) \ (K◦σA) then, by definition of “◦”, α ∈ σ ((K ∪ A)⊥⊥⊥).
Since σ ((K ∪ A)⊥⊥⊥) = σ ((K ∪ B)⊥⊥⊥) then α ∈ K ∪ B and α / ∈ K◦σB.
Therefore, (K ∪ A) \ (K◦σA) ⊆ (K ∪ B) \ (K◦σB).
(⊇) If α ∈ (K ∪ B) \ (K◦σB) then, by definition of “◦”, α ∈ σ ((K ∪ B)⊥⊥⊥).
Since σ ((K ∪ B)⊥⊥⊥) = σ ((K ∪ A)⊥⊥⊥) then α ∈ K ∪ A and α / ∈ K◦σA.
Therefore, (K ∪ B) \ (K◦σB) ⊆ (K ∪ A) \ (K◦σA)

## Partial Meet Contractions

### Definicion 7

Let K be a set of sentences and
α a sentence. Then K⊥α is the set of all K such that K ∈ K⊥α if and only if K ⊆ K,
K  α and if K ⊂ K ⊆ K then K  α. The set K⊥α is called the remainder set of K
with respect to α, and its elements are called the α-remainders of K.
For instance, if K = {p,p → q, r, r → s, r ∧ s → q,t → u} then the set of qremainders is {{p → q, r → s, r ∧ s → q,t → u}, {p, r → s, r ∧ s → q,t → u},
{p → q, r, r ∧ s → q,t → u}, {p, r, r ∧ s → q,t → u}}. The set of v-remainders of K
is equal to {K} since K  v. The set of (p → p)-remainders of K is ∅ because p → p ∈
Cn(∅) and there is no subset of K failing to imply p → p.

### Definicion 8

Let K be a set of sentences. An external selection function for K is a
function “γ ” (γ : 22L ⇒ 22L) such that for any set A ⊆ L, it holds that:
(1) γ ((K ∪ A)⊥⊥) ⊆ (K ∪ A)⊥⊥.
(2) γ ((K ∪ A)⊥⊥) = ∅.
Since every set H contains a consistent subset then H ⊥⊥ is always non-empty. For
instance, if K = {p, q, r} and A = {¬p,¬q} then
K ∪ A = {p, q, r,¬p,¬q},
(K ∪ A)⊥⊥ = {p, q, r},{¬p, q, r},{p,¬q, r},{¬p,¬q, r}
and some possible results of γ ((K ∪ A)⊥⊥) are {{¬p,¬q, r}}, {{¬p, q, r}}, {{p, q, r},
{¬p, q, r}} and {{p, q, r},{¬p, q, r},{p,¬q, r}}.

### Definicion 9

Let K be a set of sentences and γ an external selection function for K. Then
γ is an equitable selection function for K if (K ∪ A)⊥⊥⊥ = (K ∪ B)⊥⊥⊥ implies that
(K ∪ A) \ γ ((K ∪ A)⊥⊥) = (K ∪ B) \ γ ((K ∪ B)⊥⊥).

### Definicion 10

Let K and A be sets of sentences and “γ ” an equitable selection function
for K. The operator “◦” of partial meet revision by a set of sentences (◦ : 2L × 2L ⇒ 2L) es definido como K◦A = γ ((K ∪ A)⊥⊥)

### Lema 11

Si A^⊥⊥ = B^⊥⊥ entonces A = B

### Teorema 12

Let K be a belief base. The operator “◦” is a partial meet revision by a set of
sentences if and only if it satisfies inclusion, strong consistency, relevance and reversion

#### Inclusion

Straightforward from the definition.

#### Strong consistency

Since all sets in (K ∪ A)⊥⊥ are consistent, so is their intersection.

#### Relevance

Suppose that α ∈ (K ∪ A) \ (K◦γ A). That is, α ∈ K ∪ A and α / ∈ K◦γ A.
Then α / ∈  γ ((K ∪ A)⊥⊥). Since γ ((K ∪ A)⊥⊥) ⊆ (K ∪ A)⊥⊥ there is some
X such that α / ∈ X and X ∈ γ ((K ∪ A)⊥⊥). Since  γ ((K ∪ A)⊥⊥) ⊆ X then
K◦
γ A ⊆ X. Since α / ∈ X then K◦γ A ⊆ X ⊆ K ∪ A, X  ⊥ but X ∪ {α}  ⊥.
Therefore, relevance is satisfied.

#### Reversion

Suppose that K ∪A and K ∪B have the same minimally inconsistent subsets,
that is, (K ∪ A)⊥⊥⊥ = (K ∪ B)⊥⊥⊥. We need to show that (K ∪ A) \ (K◦γ A) =
(K ∪ B) \ (K◦γ B). Straightforward since γ is an equitable selection function.

### Corolario 13

Cada partial meet revision para un operador de un conjunto de sentencias es la revision kernel para el operador del conjunto de sentencias.

Each partial meet revision by a set of sentences operator is a kernel revision
by a set of sentences operator.

## Postulados que relacionan explanans y explanandum

*Sea K una base de creencias, "&omicron;" un operador de revision de creencias para K, A y B explanans, y &alpha; una sentencia del lenguaje*

### Explanans inclusion

If A  α and A ⊆ K◦A then K◦A  α.
This postulate establishes that if an agent receives an explanation for a sentence and his belief base contains this explanation then the revised belief base derives the explanandum.

### Weak success 2

If A  α and K ∪ A  ⊥ then K◦A  α.
This postulate expresses that, if an agent receives an explanation for some
sentence α and the sentences of the explanans are not rejected, then the
explanandum will be derived in the revised belief base.

### Constrained success

If A  α and K  ¬α then K◦A  α.
This postulate establishes that if an agent receives an explanation for some
sentence α not rejected in the original belief base then the explanandum will be
accepted in the revised belief base.

### Expansion

If A  α and K  α then K◦A  α.
This postulate says that if an agent accepts a sentence α and then receives a new
explanation for it he/she will continue accepting the explained sentence.

## Proposition 14

Si “◦” es un operador de revision para un conjunto de sentencias entonces satisface Explanans Inclusion.

## Proposition 15. If “◦” is an operator of revision by a set of sentences then in general it
does not satisfy neither constrained success nor expansion.


